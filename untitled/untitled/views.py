from django.views.generic import TemplateView


class IndexView(TemplateView):
    template_name = "index.html"

class CalculView(TemplateView):
        template_name = "calcul.html"